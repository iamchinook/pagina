<%@ Page Title="" Language="VB" MasterPageFile="~/index.master" AutoEventWireup="false" CodeFile="hinchada.aspx.vb" Inherits="hinchada" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">  
 <section>
	    <article style="text-align: center;">
            <p>Boca es uno de los clubes con mayor cantidad de simpatizantes en todo el mundo, junto con Flamengo, Chivas de Guadalajara, América de México, Corinthians, y superando a Juventus FC, AC Milan, Real Madrid, FC Barcelona y Manchester United.​ Además, basándose en el porcentaje que acapara en Argentina (entre 40,4% y 46,8% según diferentes encuestas), no hay ningún equipo del mundo que abarque con sus hinchas una porción tan grande de su país.</p>
            <p>Es la institución deportiva con más socios de Argentina, con 206.078 asociados en 2018.5​ Esto ha llevado a los seguidores de Boca, e incluso a la propia institución, ​a autoproclamarse como "la mitad más uno". Su relevancia se extiende más allá del continente, ya que es considerado como uno de los clubes más influyentes a nivel mundial.</p>
            <p>En una macroencuesta del diario Marca, la canción «Boca, mi buen amigo» entonada por los hinchas xeneizes, fue elegida como el cántico más espectacular del deporte a nivel mundial.​</p>
            <p>La calificación de la 12 «El jugador número doce» que se ganó la parcialidad de Boca Juniors data de 1925, con motivo de la gira europea que realizó ese año. En esa oportunidad, el equipo fue acompañado por un fanático boquense, Victoriano Caffarena, perteneciente a una familia adinerada, que financió parte de la gira. Durante la misma Caffarena ayudó al equipo en todo: hizo de técnico, de delegado y de masajista, estableciendo tal grado de relación con los jugadores, que éstos los nombraron «Jugador Número 12». Al volver a la Argentina, Caffarena era tan conocido como los mismos jugadores. Desde entonces, y ya recibido de notario, Caffarena dedicó el resto de su vida a apoyar a Boca, creando la agrupación barrial Amigos de la República de La Boca.</p> 
            <img src="la12.jpg" style="width: 400px; height: auto;" title="title" alt="boca" />
        </article>		
    </section>
</asp:Content>  

