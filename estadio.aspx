<%@ Page Title="" Language="VB" MasterPageFile="~/index.master" AutoEventWireup="false" CodeFile="estadio.aspx.vb" Inherits="estadio" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">  
  <section>
	    <article style="text-align: center;">
            <p>Desde su fundación, el club peregrinó por distintos campos de juego, casi todos ellos ubicados en el barrio de La Boca. La excepción ocurrió en 1914, año en que se consiguió un predio en la localidad de Wilde.</p>
            <p>La mudanza fue tenazmente resistida por los simpatizantes, al punto de que 1300 de los 1600 socios con que contaba Boca decidieron dejar de pagar su cuota mensual, bajando su popularidad. Inmediatamente se decidió la vuelta a La Boca, concretada en 1916. </p>
            <p>El campo ubicado en la calle Ministro Brin fue utilizado hasta 1924, año en el que se inauguró el Estadio Brandsen y Del Crucero de tablones en Brandsen y Del Crucero (actualmente Del Valle Iberlucea), utilizado hasta 1937, año en el que se comenzó a pensar en la construcción de La Bombonera. </p>
            <p>Dicho estadio fue sede del Campeonato Sudamericano 1925, Campeonato Sudamericano 1937 y Campeonato Sudamericano 1946. Durante la construcción del actual estadio el equipo jugó como local en el estadio Arquitecto Ricardo Etcheverri, propiedad del Club Ferro Carril Oeste.</p> 
            <img src="bombonera.jpg" style="width: 400px; height: auto;" title="title" alt="boca" />
        </article>		
    </section>
</asp:Content>  

