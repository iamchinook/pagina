<%@ Page Title="" Language="VB" MasterPageFile="~/index.master" AutoEventWireup="false" CodeFile="contacto.aspx.vb" Inherits="contacto" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">  
    <section>
	    <article style="text-align: center;">
		    <h1>Contacto</h1>			
		    <h3>Forma parte del Staff de la Página no Oficial de Boca Juniors</h3>
            <p>Completá el formulario y en el transcurso de unas horas nos pondremos en contacto</p>
            <strong>Asociarse al club:</strong> Actualmente la institución impide la inscripción de nuevos socios activos, por lo que te recomendamos que intentes ser adherente ingresando en el sitio<a href="http://www.bocasocios.com.ar" target="_blank">Boca Socios</a>. Ahí encontrarás toda la información.
            <p><em>Les pedimos por favor que no nos manden consultas acerca de las preguntas frecuentes. Si tiene alguna duda debe comunicarse directamente con las oficinas del club.<br></em></p>
            <p><strong>Gracias por tu aporte que nos ayuda a crecer día a día.</strong><br>Boca Juniors No Oficial</p>
    		<form>
	    		<label for="nombre">Nombre</label><br />
				<input type="text" name="nombre" value=""><br />
				<label for="email">Email</label><br />
       			<input type="text" name="email" value=""><br />
				<label for="msj">Mensaje:</label><br />
				<textarea name="msj"></textarea><br />
				<input type="submit" name="send" value="Enviar">
	    	</form>
		</article>		
	</section>
</asp:Content>  
